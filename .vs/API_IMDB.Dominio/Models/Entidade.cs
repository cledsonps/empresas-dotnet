﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace API_IMDB.Dominio.Models
{
    public abstract class Entidade
    {

        public Entidade()
        {
            Id = Guid.NewGuid();
        }
        public Guid Id { get; } //0000-0000-0000-0000
    }
}
