﻿using System.ComponentModel.DataAnnotations.Schema;

namespace API_IMDB.Dominio.Models
{
    [Table("Usuario")]
    public class Usuario : Entidade
    {
        [Column]
        public string Nome { get; set; }
        [Column]
        public string Login { get; set; }
        [Column(name:"Senha")]
        public string Senha_Hash { get; set; } // login luiz senha luiz = 939123jntkjwqkej1293i12krjkqwje1

    }
}
