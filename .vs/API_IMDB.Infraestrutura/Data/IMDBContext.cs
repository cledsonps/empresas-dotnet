﻿using Microsoft.EntityFrameworkCore;
using API_IMDB.Dominio.Models;

namespace API_IMDB.Infraestrutura.Data
{
    public class IMDBContext : DbContext
    {
        public IMDBContext(DbContextOptions<IMDBContext> options) : base(options)
        {
        }
        public DbSet<Usuario> Usuarios { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Usuario>().HasKey(key => key.Id);
        }
    }
}
