﻿using API_IMDB.Dominio.Models;
using System.Threading.Tasks;

namespace API_IMDB.Repositorio.Interface
{
    public interface IRepositorioUsuario
    {
        public Task<Usuario> CadastrarUsuario(string nome, string login, string senha_hash);
        public Usuario EditarUsuario(string nome, string login, string senha_hash);
    }
}
