﻿using API_IMDB.Dominio.Models;
using API_IMDB.Infraestrutura.Data;
using API_IMDB.Repositorio.Interface;
using System.Linq;
using System.Threading.Tasks;

namespace API_IMDB.Repositorio.Repositorio
{
    public class RepositorioUsuario : IRepositorioUsuario
    {
        private readonly IMDBContext _context;
        public RepositorioUsuario(IMDBContext context)
        {
            _context = context;
        }

        public static bool VerificaUsuario(IMDBContext context, string login)
        {
            var resultFind = context.Usuarios.Where(us => us.Login == login).FirstOrDefault();
            if (resultFind != null)
                return true;
            return false;
        }

        public static Usuario RetornaUsuario(IMDBContext context, string login)
        {
            var resultFind = context.Usuarios.Where(us => us.Login == login).FirstOrDefault();
            return resultFind;
        }

        public async Task<Usuario> CadastrarUsuario(string nome, string login, string senha_hash)
        {
            var usuario = new Usuario
            {
                Nome = nome,
                Login = login,
                Senha_Hash = senha_hash
            };
            if (!VerificaUsuario(_context, login))
            {
                _context.Add(usuario);
                await _context.SaveChangesAsync();
                return usuario;
            }
            else
            {
                usuario.Nome = ""; usuario.Senha_Hash = "";
                usuario.Login = string.IsNullOrEmpty(login) ? "" : login;
                return usuario;
            }
        }

        public Usuario EditarUsuario(string nome, string login, string senha_hash)
        {
            var usuarioRetorn = RetornaUsuario(_context, login);

            usuarioRetorn = new Usuario
            {
                Nome = nome,
                Login = login,
                Senha_Hash = senha_hash
            };

            _context.Update(usuarioRetorn);
            _context.SaveChangesAsync();
            return usuarioRetorn;

        }

    }
}
