﻿using API_IMDB.Dominio.Models;
using API_IMDB.Infraestrutura.Data;
using API_IMDB.Repositorio.Interface;
using API_IMDB.Repositorio.Repositorio;
using API_IMDB.Servicos.Interface;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace API_IMDB.Servicos.Services
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IRepositorioUsuario _repositorio;
        private readonly IMDBContext _context;
        public UsuarioService(IRepositorioUsuario repo, IMDBContext context)
        {
            _repositorio = repo;
            _context = context;
        }

        public static string MessageUsuario(string nome, string login, string senha_hash)
        {
            var validaMsg = "Todos os campos são obrigatórios !";
            var idBool = false;

            if (!string.IsNullOrEmpty(nome) && !string.IsNullOrEmpty(login)
                && !string.IsNullOrEmpty(senha_hash))
            {
                validaMsg = "Usuário cadastrado com Sucesso !";
                idBool = true;
            }
            if (!string.IsNullOrWhiteSpace(login) && idBool == false)
                validaMsg = "Este Login já está em uso !";

            return validaMsg;
        }

        public static bool Idbool(string nome, string login, string senha_hash)
        {
            if (!string.IsNullOrEmpty(nome) && !string.IsNullOrEmpty(login)
                && !string.IsNullOrEmpty(senha_hash))
                return true;
            return false;
        }

        public async Task<Usuario> CadastrarUsuario(string nome, string login, string senha_hash)
        {
            var usuarioCadastrado = new Usuario();
            if (!string.IsNullOrEmpty(nome) && !string.IsNullOrEmpty(login) && !string.IsNullOrEmpty(senha_hash))
            {
                byte[] salt = new byte[128 / 8];
                using (var rng = RandomNumberGenerator.Create())
                {
                    rng.GetBytes(salt);
                }
                var hash = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                           password: senha_hash,
                           salt: salt,
                           prf: KeyDerivationPrf.HMACSHA1,
                           iterationCount: 10000,
                           numBytesRequested: 256 / 8));
                usuarioCadastrado = await _repositorio.CadastrarUsuario(nome, login, hash);
                return usuarioCadastrado;
            }
            else
            {
                usuarioCadastrado.Nome = ""; usuarioCadastrado.Login = ""; usuarioCadastrado.Senha_Hash = "";
                return usuarioCadastrado;
            }
        }

        public Usuario EditarUsuario(string nome, string login, string senha_hash)
        {
            var usuarioEditado = new Usuario();
            if (!string.IsNullOrEmpty(nome) && !string.IsNullOrEmpty(login) && !string.IsNullOrEmpty(senha_hash))
            {
                usuarioEditado = _repositorio.EditarUsuario(nome, login, senha_hash);
                return usuarioEditado;
            }
            else
            {
                usuarioEditado.Nome = ""; usuarioEditado.Login = ""; usuarioEditado.Senha_Hash = "";
                return usuarioEditado;
            }
        }


    }
}
