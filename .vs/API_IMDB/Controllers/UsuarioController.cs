﻿using API_IMDB.DTO;
using API_IMDB.Servicos.Interface;
using API_IMDB.Servicos.Services;
using API_IMDB.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API_IMDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly IUsuarioService _usuarioService;

        public UsuarioController(IUsuarioService usuarioService)
        {
            _usuarioService = usuarioService;
        }
        // GET: api/<UsuarioController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<UsuarioController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<UsuarioController>/cadastro
        [HttpPost("cadastro")]
        public async Task<ActionResult> CadastrarUsuario(UsuarioCadastroDTO usuario)
        {
            // FluentValidation
            // AutoMapper
            if(usuario != null)
            {
                var usuarioCadastrado = await _usuarioService
                    .CadastrarUsuario(usuario.Nome, usuario.Login, usuario.Senha);

                var idBool = UsuarioService.Idbool(usuarioCadastrado.Nome, usuarioCadastrado.Login, usuario.Senha);
                var validaMsg = UsuarioService.MessageUsuario(usuarioCadastrado.Nome, usuarioCadastrado.Login, usuario.Senha);

                return Created("", new UsuarioViewModel
                {
                    Id = idBool == true ? usuarioCadastrado.Id.ToString() : null,
                    Nome = usuarioCadastrado.Nome,
                    Login = usuarioCadastrado.Login,
                    Senha = "",
                    Mensagem = validaMsg
                });
            }
            else
            {
                return BadRequest();
            }
        }

        // PUT api/<UsuarioController>/Edicao/5
        [HttpPut("Edicao/{login}")]
        public ActionResult EditarUsuario(UsuarioCadastroDTO usuario)
        {
            if (usuario != null)
            {
                var usuarioCadastrado = _usuarioService
                    .EditarUsuario(usuario.Nome, usuario.Login, usuario.Senha);

                var idBool = UsuarioService.Idbool(usuarioCadastrado.Nome, usuarioCadastrado.Login, usuario.Senha);
                var validaMsg = UsuarioService.MessageUsuario(usuarioCadastrado.Nome, usuarioCadastrado.Login, usuario.Senha);

                return Created("", new UsuarioViewModel
                {
                    Id = idBool == true ? usuarioCadastrado.Id.ToString() : null,
                    Nome = usuarioCadastrado.Nome,
                    Login = usuarioCadastrado.Login,
                    Senha = "",
                    Mensagem = validaMsg
                });
            }
            else
            {
                return BadRequest();
            }
        }

        // DELETE api/<UsuarioController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
