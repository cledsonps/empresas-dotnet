﻿namespace API_IMDB.DTO
{
    public class UsuarioCadastroDTO
    {
        public string Nome { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
    }
}
